<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\ConfigDataModel;

/**
 * 系统配置-服务类
 * @author 牧羊人
 * @since 2022/2/13
 */
class ConfigWebService extends BaseService
{
    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new ConfigDataModel();
    }

    /**
     * 获取配置信息
     * @return array
     * @since 2022/3/5
     * @author 牧羊人
     */
    public function getList()
    {
        // 获取配置列表
        $configList = $this->model
            ->where(["mark" => 1])
            ->order("sort asc")
            ->select();
        $list = [];
        if ($configList) {
            $configDataModel = new ConfigDataModel();
            foreach ($configList as &$val) {
                $dataList = $configDataModel
                    ->where([
                        "config_id" => $val['id'],
                        "mark" => 1,
                    ])
                    ->order("sort asc")
                    ->select();
                foreach ($dataList as &$v) {
                    if ($v['type'] == "array" || $v['type'] == "radio" || $v['type'] == "checkbox" || $v['type'] == "select") {
                        $data = preg_split('/[\r\n]+/s', $v['options']);
                        if ($data) {
                            $arr = [];
                            foreach ($data as $vt) {
                                $value = preg_split('/[:：]+/s', $vt);
                                $arr[$value[0]] = $value[1];
                            }
                            $v['param'] = $arr;
                        }
                        // 复选框
                        if ($v['type'] == "checkbox") {
                            $v['value'] = explode(",", $v['value']);
                        }
                    }
                    // 单图
                    if ($v['type'] == "image" && !empty($v['value'])) {
                        $v['value'] = get_image_url($v['value']);
                    }
                    // 多图
                    if ($v['type'] == "images") {
                        $urlList = explode(",", $v['value']);
                        $itemList = [];
                        foreach ($urlList as $vt) {
                            if (empty($vt)) {
                                continue;
                            }
                            $itemList[] = get_image_url($vt);
                        }
                        $v['value'] = $itemList;
                    }
                }
                $item = array();
                $item['config_id'] = $val['id'];
                $item['config_name'] = $val['name'];
                $item['item_list'] = $dataList;
                $list[] = $item;
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 保存配置信息
     * @return array
     * @since 2022/3/5
     * @author 牧羊人
     */
    public function edit()
    {
        // 参数
        $data = I('post.');
        if (!$data) {
            return message("参数不能为空", false);
        }
        foreach ($data as $key => &$val) {
            // 图片处理
            $preg = "/^http(s)?:\\/\\/.+/";
            if (is_string($val) && preg_match($preg, $val)) {
                if (strpos($val, "temp") !== false) {
                    $val = save_image($val, 'config');
                } else {
                    $val = str_replace(IMG_URL, "", $val);
                }
            }
            if (is_array($val)) {
                $item = [];
                foreach ($val as $vt) {
                    $preg = "/^http(s)?:\\/\\/.+/";
                    if (preg_match($preg, $vt)) {
                        if (strpos($vt, "temp") !== false) {
                            $vt = save_image($vt, 'config');
                        } else {
                            $vt = str_replace(IMG_URL, "", $vt);
                        }
                        $item[] = $vt;
                    } else {
                        $item[] = $vt;
                    }
                }
                $val = !empty($item) ? implode(",", $item) : "";
            }
            $configDataModel = new ConfigDataModel();
            $result = $configDataModel->where(["code" => $key])->find();
            $info = [];
            $info['id'] = $result['id'];
            $info['value'] = !empty($val) ? $val : "";
            $configDataModel->edit($info);
        }
        return message();
    }
}