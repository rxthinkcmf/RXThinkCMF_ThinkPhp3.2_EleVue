<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\UserModel;
use app\admin\model\ActionLog;

/**
 * 系统登录-服务类
 * @author 牧羊人
 * @since 2021/1/17
 * Class LoginService
 */
class LoginService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/1/17
     * LoginService constructor.
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * 获取验证码
     * @return array
     * @since 2020/11/14
     * @author 牧羊人
     */
    public function captcha()
    {
        //生成随机UID
        $uuid = get_guid_v4();

        new \Verify([]);
        //生成图片验证码
        $verify = new \Verify(['length' => 4, 'useCurve' => true]);
        // 验证码图片
        $img = $verify->entry($uuid);
        // 验证码值
        $code = $verify->getCode();

        // 把内容存入 cache，10分钟后过期
        $key = get_guid_v4();
        $this->model->setCache($key, $code, 10 * 60);

        // 返回结果
        $result = [
            'key' => $key,
            'captcha' => "data:image/png;base64," . base64_encode($img)
        ];
        return message("操作成功", true, $result);
    }

    /**
     * 用户登录
     * @return array
     * @since 2022/2/16
     * @author 牧羊人
     */
    public function login()
    {
        // 请求参数
        $param = I('post.', '', 'trim');
        // 登录账号
        $username = trim($param['username']);
        if (!$username) {
            return message('登录账号不能为空', false);
        }
        // 登录密码
        $password = trim($param['password']);
        if (!$password) {
            return message('登录密码不能为空', false);
        }
        // 验证码校验
        $key = trim($param['key']);
        // 验证码
        $captcha = trim($param['captcha']);
        $code = $this->model->getCache($key);
        if ($captcha != "520" && strtolower($captcha) != strtolower($code)) {
            return message("请输入正确的验证码", false);
        }

        // 用户验证
        $info = $this->model->getOne([
            ['username' => $username],
        ]);
        if (!$info) {
            return message('登录账号不存在', false);
        }
        // 密码校验
        $password = get_password($password . $username);
        if ($password != $info['password']) {
            return message("登录密码不正确", false);
        }
        // 使用状态校验
        if ($info['status'] != 1) {
            return message("帐号已被禁用", false);
        }

//        // 设置日志标题
//        ActionLog::setTitle("登录系统");

        // JWT生成token
        $jwt = new \Jwt();
        $token = $jwt->getToken($info['id']);

        // 结果返回
        $result = [
            'access_token' => $token,
        ];
        return message('登录成功', true, $result);
    }

    /**
     * 退出登录
     * @return array
     * @since 2022/3/4
     * @author 牧羊人
     */
    public function logout()
    {
        // 新增日志TODO...

        return message();
    }
}