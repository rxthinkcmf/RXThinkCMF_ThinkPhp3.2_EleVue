<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Admin\Model\UserModel;
use Admin\Service\MenuService;
use Admin\Service\UserPermissionService;
use Admin\Service\UserService;

/**
 * 后台主页-控制器
 * @author 牧羊人
 * @since 2021/1/17
 * Class IndexController
 * @package Admin\Controller
 */
class IndexController extends BaseController
{
    /**
     * 初始化
     * @author 牧羊人
     * @since 2021/1/17
     */
    protected function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    /**
     * 获取菜单列表
     * @author 牧羊人
     * @since 2022/3/2
     */
    public function getMenuList()
    {

        // 获取权限菜单
        $menuService = new MenuService();
        $menuList = $menuService->getPermissionMenuList($this->userId);
        $this->ajaxReturn($menuList);
    }

    /**
     * 获取用户信息
     * @author 牧羊人
     * @since 2022/3/3
     */
    public function getUserInfo()
    {
        $userService = new UserService();
        $result = $userService->getUserInfo($this->userId);
        $this->ajaxReturn($result);
    }

    /**
     * 个人中心
     * @return array|void
     * @since 2022/2/17
     * @author 牧羊人
     */
    public function updateUserInfo()
    {
        if (IS_POST) {
            // 参数
            $param = I('post.', '', 'trim');
            // 真实姓名
            $realname = trim($param['realname']);
            // 邮箱
            $email = trim($param['email']);
            // 个人简介
            $intro = trim($param['intro']);
            // 街道地址
            $address = trim($param['address']);
            // 联系电话
            $mobile = trim($param['mobile']);
            $data = [
                'id' => $this->userId,
                'realname' => $realname,
                'email' => $email,
                'intro' => $intro,
                'address' => $address,
                'mobile' => $mobile,
            ];
            $userModel = new UserModel();
            $result = $userModel->edit($data);
            if (!$result) {
                $this->ajaxReturn(message("信息保存失败", false));
                return;
            }
            $this->ajaxReturn(message());
            return;
        }
        // 获取用户信息
        $userModel = new UserModel();
        $userInfo = $userModel->getInfo($this->userId);
        $this->assign("userInfo", $userInfo);
        return $this->render();
    }

    /**
     * 修改密码
     * @return array|void
     * @since 2022/2/17
     * @author 牧羊人
     */
    public function updatePwd()
    {
        if (IS_POST) {
            // 参数
            $param = I('post.', '', 'trim');
            // 原密码
            $oldPassword = trim($param['oldPassword']);
            if (!$oldPassword) {
                return message("原密码不能为空", false);
            }
            // 新密码
            $newPassword = trim($param['newPassword']);
            if (!$newPassword) {
                return message("新密码不能为空", false);
            }
            // 确认密码
            $rePassword = trim($param['rePassword']);
            if (!$rePassword) {
                return message("确认密码不能为空", false);
            }
            if ($newPassword != $rePassword) {
                return message("两次输入的密码不一致", false);
            }
            if (get_password($oldPassword . $this->userInfo['username']) != $this->userInfo['password']) {
                return message("原始密码不正确", false);
            }
            // 设置新密码
            $data = [
                'id' => $this->userId,
                'password' => get_password($newPassword . $this->userInfo['username']),
            ];
            $userModel = new UserModel();
            $result = $userModel->edit($data);
            if (!$result) {
                $this->ajaxReturn(message("修改失败", false));
                return;
            }
            $this->ajaxReturn(message("修改密码成功"));
        }
    }

}