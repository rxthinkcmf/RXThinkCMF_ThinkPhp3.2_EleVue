<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace Admin\Service;

use Admin\Model\MenuModel;
use Admin\Model\UserModel;

/**
 * 菜单-服务类
 * @author 牧羊人
 * @since 2021/1/17
 * Class MenuService
 * @package Admin\Service
 */
class MenuService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/1/17
     * MenuService constructor.
     */
    public function __construct()
    {
        $this->model = new MenuModel();
    }

    /**
     * 获取菜单列表
     * @return array
     * @since 2021/5/16
     * @author 牧羊人
     */
    public function getList()
    {
        // 参数
        $param = I("request.");

        // 查询条件
        $map = [];
        // 菜单标题
        $title = getter($param, "title");
        if ($title) {
            $map['title'] = array('like', "%{$title}%");
        }
        $list = $this->model->getList($map, 'sort asc');
        return message("操作成功", true, $list);
    }

    public function info()
    {
        // 参数
        $id = I("get.id", 0);
        $info = [];
        if ($id) {
            $info = $this->model->getInfo($id);
        }
        // 获取权限节点
        $checkedList = array();
        if ($info['type'] == 0) {
            $permissionList = $this->model
                ->field("sort")
                ->where(array([
                    'pid' => $id,
                    'type' => 1,
                    'mark' => 1
                ]))
                ->select();
            if (is_array($permissionList)) {
                $checkedList = array_key_value($permissionList, "sort");
            }
            $info['checkedList'] = $checkedList;
        }
        return message("操作成功", true, $info);
    }

    /**
     * 添加或编辑
     * @return array
     * @throws \think\Exception
     * @author 牧羊人
     * @since 2022/3/5
     */
    public function edit()
    {
        // 参数
        $param = I('post.', '', 'trim');
        // 权限节点
        $checkedList = isset($param['checkedList']) ? $param['checkedList'] : array();
        unset($param['checkedList']);
        // 保存数据
        $result = $this->model->edit($param);
        if (!$result) {
            return message("操作失败", false);
        }

        // 设置权限节点
        $isFunc = false;
        if ($param['type'] == 0 && !empty($param['path']) && !empty($checkedList)) {
            $item = explode("/", $param['path']);
            // 模块名称
            $moduleName = $item[count($item) - 1];
            // 模块标题
            $moduleTitle = str_replace("管理", "", $param['title']);
            // 删除以存在的节点
            $funcIds = $this->model->where([
                'pid' => $result,
                'type' => 1,
            ])->column("id");
            $this->model->deleteDAll($funcIds, true);
            $isFunc = true;
            // 遍历权限节点
            foreach ($checkedList as $val) {
                $data = [];
                $data['pid'] = $result;
                $data['type'] = 1;
                $data['status'] = 1;
                $data['sort'] = intval($val);
                $data['target'] = $param['target'];

                // 判断当前节点是否已存在
                $permissionInfo = $this->model->where([
                    'pid' => $result,
                    'type' => 1,
                    'sort' => $val,
                    'mark' => 1
                ])->find();
                if ($permissionInfo) {
                    $data['id'] = $permissionInfo ? $permissionInfo['id'] : 0;
                }
                if ($val == 1) {
                    // 查询
                    $data['title'] = "查询" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/index";
                    $data['permission'] = "sys:{$moduleName}:index";
                } else if ($val == 5) {
                    // 添加
                    $data['title'] = "添加" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/edit";
                    $data['permission'] = "sys:{$moduleName}:add";
                } else if ($val == 10) {
                    // 修改
                    $data['title'] = "修改" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/edit";
                    $data['permission'] = "sys:{$moduleName}:edit";
                } else if ($val == 15) {
                    // 删除
                    $data['title'] = "删除" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/delete";
                    $data['permission'] = "sys:{$moduleName}:delete";
                } else if ($val == 20) {
                    // 详情
                    $data['title'] = $moduleTitle . "详情";
                    $data['path'] = "/{$moduleName}/detail";
                    $data['permission'] = "sys:{$moduleName}:detail";
                } else if ($val == 25) {
                    // 状态
                    $data['title'] = "设置状态";
                    $data['path'] = "/{$moduleName}/status";
                    $data['permission'] = "sys:{$moduleName}:status";
                } else if ($val == 30) {
                    // 批量删除
                    $data['title'] = "批量删除";
                    $data['path'] = "/{$moduleName}/dall";
                    $data['permission'] = "sys:{$moduleName}:dall";
                } else if ($val == 35) {
                    // 添加子级
                    $data['title'] = "添加子级";
                    $data['path'] = "/{$moduleName}/addz";
                    $data['permission'] = "sys:{$moduleName}:addz";
                } else if ($val == 40) {
                    // 全部展开
                    $data['title'] = "全部展开";
                    $data['path'] = "/{$moduleName}/expand";
                    $data['permission'] = "sys:{$moduleName}:expand";
                } else if ($val == 45) {
                    // 全部折叠
                    $data['title'] = "全部折叠";
                    $data['path'] = "/{$moduleName}/collapse";
                    $data['permission'] = "sys:{$moduleName}:collapse";
                } else if ($val == 50) {
                    // 导出数据
                    $data['title'] = "导出" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/export";
                    $data['permission'] = "sys:{$moduleName}:export";
                } else if ($val == 55) {
                    // 导入数据
                    $data['title'] = "导入" . $moduleTitle;
                    $data['path'] = "/{$moduleName}/import";
                    $data['permission'] = "sys:{$moduleName}:import";
                } else if ($val == 60) {
                    // 分配权限
                    $data['title'] = "分配权限";
                    $data['path'] = "/{$moduleName}/permission";
                    $data['permission'] = "sys:{$moduleName}:permission";
                } else if ($val == 65) {
                    // 分配权限
                    $data['title'] = "重置密码";
                    $data['path'] = "/{$moduleName}/resetPwd";
                    $data['permission'] = "sys:{$moduleName}:resetPwd";
                }
                if (empty($data['title'])) {
                    continue;
                }
                $menuModel = new MenuModel();
                $menuModel->edit($data);
            }
        }
        return message("操作成功", true, $isFunc);
    }

    /**
     * 获取权限菜单列表
     * @param $userId 用户ID
     * @return array|bool|mixed|string|null
     * @author 牧羊人
     * @since 2021/1/17
     */
    public function getPermissionMenuList($userId)
    {
        if ($userId == 1) {
            // 管理员(拥有全部权限)
            $menuModel = new MenuModel();
            $menuList = $menuModel->getChilds(0);
            return message("操作成功", true, $menuList);
        } else {
            // 非管理员
            $menuList = $this->model->getPermissionMenuList($userId, 0);
            return message("操作成功", true, $menuList);
        }
    }

    /**
     * 获取权限节点列表
     * @param $userId 用户ID
     * @return array
     * @author 牧羊人
     * @since 2022/2/16
     */
    public function getPermissionFuncList($userId)
    {
        if ($userId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->where([
                'type' => 1,
                'mark' => 1,
            ])->field("permission")->select();
            $permissionList = array_key_value($menuList, "permission");
            return $permissionList;
        } else {
            // 非管理员
            $permissionList = $this->model->getPermissionFuncList($userId);
            return $permissionList;
        }
    }

}